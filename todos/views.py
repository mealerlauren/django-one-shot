from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoListForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {"todo_list_list": todo_list}
    return render(request, "todos/todo_list_list.html", context)



def todo_list_detail(request, id):
    lists = TodoList.objects.get(id=id)
    context = {"todo_list_detail":lists}
    return render(request, "todos/todo_list_detail.html", context)



def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/todo_list_create.html", context)



def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {
        "form": form,
        "todo_list": todo_list,
    }
    return render(request, "todos/todo_list_update.html", context)



def todo_list_delete(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")

    context = {
        "todo_list": todo_list,
    }

    return render(request, "todos/todo_list_delete.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list_id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }

    return render(request, "todos/todo_item_create.html", context)
